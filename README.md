# Insta.Memory

Instagram bot for collecting WWII photos and messaging with users who posted it

Was developed in "Memory.Hack"

## How does it works?

1. Any user published photo in instagram with hashtags related to WWII memory events in Russia
2. Our .NET Core service monitoring all new photos with mentioned hashtags
3. If there are any new photos then bot download it and collect data with MongoDB
4. After it, bot will write this user into Direct with suggestion to add his photo into centralized database of WWII veterans

##### Prototype
![gif prototype](insta_memory.gif)
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InstaParser.DB.Models
{
    public class Post
    {
        public string Id;

        public List<string> ImageUrls = new List<string>();

        public static List<Post> GetPostsByIds(IEnumerable<string> postIds) => postIds.Select(i => new Post() { Id = i }).ToList();
    }
}

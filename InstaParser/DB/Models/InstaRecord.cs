﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InstaParser.DB.Models
{
    public class InstaRecord
    {
        public string UserId;

        public List<Post> Posts = new List<Post>();

        public bool IsWritten = false;
    }
}

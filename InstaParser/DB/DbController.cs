﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using InstaParser.DB.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace InstaParser.DB
{
    public static class DbController
    {
        #region Consts
        private const string ConnectionString = "mongodb://127.0.0.1:27017";
        private const string DataBaseName = "memoryDb";
        private const string CollectionName = "instaCollection";
        #endregion

        private static IMongoCollection<BsonDocument> GetInstaCollection()
        {
            //start connect
            var client = new MongoClient(ConnectionString);

            var database = client.GetDatabase(DataBaseName);
            return database.GetCollection<BsonDocument>(CollectionName);
        }

        public static void InsertInstaRecords(IEnumerable<InstaRecord> records)
        {
            if (!records.Any())
                return;

            var collection = GetInstaCollection();

            //prepare data
            var preparedData = new List<BsonDocument>();

            foreach (var record in records)
            {
                preparedData.Add(new BsonDocument()
                {
                    {"UserId", record.UserId },
                    {"PostIds", new BsonArray(record.Posts.Select(p=>p.Id))},
                    {"IsWritten", record.IsWritten },
                });

                ////todo improve hotfix
                //try
                //{
                //    collection.InsertOne(preparedData.Last());
                //}
                //catch (Exception e)
                //{
                //    //Console.Write(e.Message);
                //}
            }

            //todo check
            //todo change to InsertManyAsync
            //add data
            try
            {
                //{ ordered: false}
                collection.InsertMany(preparedData, new InsertManyOptions() { IsOrdered = false });
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        //todo improve
        public static IEnumerable<InstaRecord> SelectInstaRecords(bool isWritten = false)
        {
            var collection = GetInstaCollection();

            //get data
            //todo change to FindAsync
            var filter = Builders<BsonDocument>.Filter.Eq("IsWritten", isWritten);
            var preparedData = collection.Find(filter).ToList();

            //convert data
            var instaRecords = new List<InstaRecord>();

            foreach (var data in preparedData)
            {
                instaRecords.Add(new InstaRecord()
                {
                    UserId = data["UserId"].ToString(),
                    Posts = Post.GetPostsByIds(data["PostIds"].AsBsonArray.Select(p => p.AsString)),
                    IsWritten = data["IsWritten"].ToBoolean()
                });
            }

            return instaRecords;
        }

        public static void ChangeInstaUserStatus(InstaRecord record, bool isWritten = true)
        {
            var collection = GetInstaCollection();

            var filter = Builders<BsonDocument>.Filter.Eq("UserId", record.UserId);
            var update = Builders<BsonDocument>.Update.Set("IsWritten", isWritten);

            collection.UpdateOne(filter, update);
        }

        public static void DropInstaCollection()
        {
            var client = new MongoClient(ConnectionString);

            var database = client.GetDatabase(DataBaseName);

            database.DropCollection(CollectionName);
        }
    }
}
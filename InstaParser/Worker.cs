using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using InstaParser.DB;
using InstaParser.DB.Models;
using InstaSharper.Classes.Models;
using InstaParser.Insta;
using InstaParser.ChatHelper;

namespace InstaParser
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // ⚠️ Warning! Drop it manually, because need to add index after.
            // if (InstaConfig.DemoMode)
            // {
            //     DbController.DropInstaCollection();
            //     Console.WriteLine("DB is dropped");
            //     return;
            // }

            if (!InstaConfig.ParserLoop)
            {
                await RunParsing(InstaConfig.DemoMode);
            }
            else
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                    await RunParsing(InstaConfig.DemoMode);
                    // await InstaHelper.SendNextMessagesToAll();
                    await Task.Delay(5000, stoppingToken);
                }
            }
        }

        public static async Task RunParsing(bool demo = false)
        {
            var successLogin = await InstaHelper.Login();
            if (!successLogin)
            {
                return;
            }

            var records = await InstaHelper.GetMediaFeed(demo ? InstaConfig.DemoTags : InstaConfig.ParseTags);
            var alreadyAwareUsers = DbController.SelectInstaRecords(isWritten: true).Select(x => x.UserId).ToHashSet();
            records = records.Where(x => !alreadyAwareUsers.Contains(x.UserId)).ToList();
            DbController.InsertInstaRecords(records);

            var newRecords = DbController.SelectInstaRecords(isWritten: false);
            Console.WriteLine($"New users - {newRecords.Count()}");
            foreach (var record in newRecords)
            {
                Console.WriteLine($"Sending message and loading image for user {record.UserId}");
                var successSend = false;

                // ⚠️ Warning! Remove `demo` key only if 100% sure to send message to the real users.
                if (demo)
                {
                    var messages = new string[] { InstaConfig.WelcomeMessage, ChatHelper.DecisionTree.Questions.First().Text };
                    successSend = await InstaHelper.SendMessage(record.UserId, messages: messages);
                    Console.WriteLine(successSend ? $"Successfully sent messages to user {record.UserId}"
                                                  : $"Failed to send messages to user {record.UserId} ");
                }

                if (successSend && InstaConfig.LoadImages)
                {
                    foreach (var post in record.Posts)
                    {
                        var imageUrl = await InstaHelper.GetPostImageUrl(post.Id);
                        await ImageManager.ImageManager.SaveImage(record.UserId, post.Id, imageUrl);
                        Console.WriteLine($"Images from post {post.Id} is loaded");
                    }
                }

                DbController.ChangeInstaUserStatus(record, isWritten: successSend);
            }

            // if (demo) {
            //     var demoUserId = await insta.GetUserId(InstaConfig.DemoUsername);
            //     await insta.SendMessage(demoUserId, InstaConfig.WelcomeMessage);
            // }
        }
    }
}

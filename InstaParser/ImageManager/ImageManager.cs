﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InstaParser.ImageManager
{
    public static class ImageManager
    {
        #region Const
        private const string OutputFolder = "InstaImage";
        private const string DefaultImagePrefix = "memImage";
        private const string DefaultImageType = ".jpg";
        #endregion

        public static async Task SaveImage(string userId, string postId, string imageUrl)
        {
            using (var client = new WebClient())
            {
                var totalImagesDirectory = CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), OutputFolder));
                var userDirectory = CreateDirectory(Path.Combine(totalImagesDirectory, userId));
                var postDirectory = CreateDirectory(Path.Combine(userDirectory, postId));
                
                await Task.Run(() =>
                    client.DownloadFileAsync(new Uri(imageUrl), GetImageName(postDirectory))
                );
            } 
        }

        private static string CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return directory;
        }

        private static string GetImageName(string postDirectory)
        {
            var i = 0;

            var imageName = $"{DefaultImagePrefix}_{i}{DefaultImageType}";
            var imagePath = Path.Combine(postDirectory, imageName);

            while (File.Exists(imagePath))
            {
                imageName = $"{DefaultImagePrefix}{++i}{DefaultImageType}";
                imagePath = Path.Combine(postDirectory, imageName);
            }

            return imagePath;
        }
    }
}
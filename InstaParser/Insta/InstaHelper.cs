using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using InstaSharper.API;
using InstaSharper.API.Builder;
using InstaSharper.Classes;
using InstaSharper.Classes.Models;
using InstaSharper.Logger;
using InstaParser.DB.Models;

namespace InstaParser.Insta {

    public class InstaHelper {

        private static IInstaApi _instaApi;

        public static async Task<bool> Login() {
            
            // create user session data and provide login details
            var userSession = new UserSessionData
            {
                UserName = InstaConfig.BotUsername,
                Password = InstaConfig.BotPassword
            };

            var delay = RequestDelay.FromSeconds(2, 2);
            // create new InstaApi instance using Builder
            _instaApi = InstaApiBuilder.CreateBuilder()
                .SetUser(userSession)
                .UseLogger(new DebugLogger(LogLevel.Exceptions)) // use logger for requests and debug messages
                .SetRequestDelay(delay)
                .Build();
            

            const string stateFile = "state.bin";
            try
            {
                if (File.Exists(stateFile))
                {
                    Console.WriteLine("Loading state from file");
                    using (var fs = File.OpenRead(stateFile))
                    {
                        _instaApi.LoadStateDataFromStream(fs);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (!_instaApi.IsUserAuthenticated)
            {
                // login
                Console.WriteLine($"Logging in as {userSession.UserName}");
                delay.Disable();
                var logInResult = await _instaApi.LoginAsync();
                delay.Enable();
                if (!logInResult.Succeeded)
                {
                    Console.WriteLine($"Unable to login: {logInResult.Info.Message}");
                    return false;
                }
            }
            var state = _instaApi.GetStateDataAsStream();
            using (var fileStream = File.Create(stateFile))
            {
                state.Seek(0, SeekOrigin.Begin);
                state.CopyTo(fileStream);
            }

            return true;
        }

        public static async Task<bool> Logout() {
            var logout = await _instaApi.LogoutAsync();
            return logout.Succeeded;
        }

        public static async Task<List<InstaHashtag>> SimilarTags(string tag) {
            var result = await _instaApi.SearchHashtag(tag);
            return result.Succeeded ? result.Value : new List<InstaHashtag>();
        }

        public static async Task<List<InstaRecord>> GetMediaFeed(string[] tags = null, int? pages = null, bool filtered = true) {
            var records = new List<InstaRecord>();
            var feed = new List<InstaMedia>();
            var hashtags = tags ?? InstaConfig.ParseTags;
            var tagPages = pages ?? InstaConfig.DefaultFeedPages;

            Console.WriteLine("Loading media feed by tags");
            foreach (var hashtag in hashtags)
            {
                var tagFeed = await GetMediaFeed(hashtag, tagPages);
                feed.AddRange(tagFeed);
            }

            if (feed.Count == 0) {
                Console.WriteLine($"Empty feed for tags [{string.Join(", ", tags)}]");
                return records;
            }
            else if (!feed.Any((post) => post.Images.Count > 0)) {
                Console.WriteLine($"No images in feed for tags [{string.Join(", ", tags)}]");
                feed.Clear();
                return records;
            }

            // TODO: Filter by post content and portrait image

            var userPosts = new Dictionary<string, List<InstaMedia>>();
            foreach (var media in feed)
            {
                if (media.Images.Count == 0)
                {
                    continue;
                }

                var userId = media.User.Pk.ToString();
                if (userPosts.ContainsKey(userId) && !userPosts[userId].Any(m => m.InstaIdentifier == media.InstaIdentifier))
                {
                    userPosts[userId].Add(media);
                }
                else
                {
                    userPosts.TryAdd(userId, new List<InstaMedia>() { media });
                }
            }

            Console.WriteLine("\nLoaded posts");
            foreach (var userPost in userPosts)
            {
                Console.WriteLine($"user: {userPost.Key}");
                var posts = new List<Post>();
                foreach (var media in userPost.Value)
                {
                    Console.WriteLine($"\t| post: {media.InstaIdentifier}");
                    var images = media.Images;
                    if (images.Count > 1)
                    {
                        var maxWidth = images.Max(x => x.Width);
                        images.RemoveAll(x => x.Width < maxWidth);
                    }
                    var post = new Post()
                    {
                        Id = media.InstaIdentifier,
                        ImageUrls = images.Select(url => url.URI).ToList() // TODO in DB
                    };
                    // Console.WriteLine($"\t\t| images: [{string.Join(", ", post.ImageUrls)}]");
                    posts.Add(post);
                }
                var record = new InstaRecord()
                {
                    UserId = userPost.Key,
                    Posts = posts
                };

                records.Add(record);
            }

            return records;
        }

        private static async Task<List<InstaMedia>> GetMediaFeed(string tag, int pages = 1) {
            var tagFeed = await _instaApi.GetTagFeedAsync(tag, PaginationParameters.MaxPagesToLoad(pages));
            return tagFeed.Succeeded ? tagFeed.Value.Medias : new List<InstaMedia>();
        }

        public static async Task SendNextMessagesToAll(bool demo = false) {
            var threads = await InstaHelper.GetDirectThreads();
            foreach (var thread in threads)
            {
                var nextMessage = ChatHelper.ChatHelper.GetNextMessage(thread.Items);
                if (demo && !thread.Items.Any(m => m.UserId == InstaConfig.BotId))
                {
                    await InstaHelper.SendMessage(thread, InstaConfig.WelcomeMessage);
                }
                if (!string.IsNullOrWhiteSpace(nextMessage))
                {
                    await InstaHelper.SendMessage(thread, InstaConfig.WelcomeMessage);
                }
            }
        }

        public static async Task<bool> SendMessage(string userId, string[] messages) {
            var result = true;
            foreach (var message in messages)
            {
                result = result && await SendMessage(userId, message);
            }
            return result;
        }

        private static TimeSpan sendMessageDelay = new TimeSpan(0, 0, 5);
        private static async Task<bool> SendMessage(string userId, string message) {
            await Task.Delay(sendMessageDelay);
            var result = await _instaApi.SendDirectMessage(userId, string.Empty, message);
            return result.Succeeded;
        }

        private static async Task<bool> SendMessage(InstaDirectInboxThread thread, string message) {
            await Task.Delay(sendMessageDelay);
            var result = await _instaApi.SendDirectMessage($"{thread.Users.FirstOrDefault()?.Pk}", thread.ThreadId, message);
            return result.Succeeded;
        }

        private static async Task<List<InstaDirectInboxThread>> GetDirectThreads() {
            var result = await _instaApi.GetDirectInboxAsync();
            if (!result.Succeeded)
            {
                return new List<InstaDirectInboxThread>();
            }
            var threads = result.Value.Inbox.Threads;

            var needApproveThreads = threads.Where(x => x.Pending || x.IsSpam).ToList();
            if (needApproveThreads.Any())
            {
                foreach (var thread in needApproveThreads)
                {
                    await _instaApi.ApprovePendingDirectThread(thread.ThreadId);
                }
            }
            return threads;
        }

        public static async Task<string> GetUserId(string username) {
            var user = await _instaApi.GetUserAsync(username);
            return user.Succeeded ? user.Value.Pk.ToString() : null;
        }

        public static async Task<string> GetPostImageUrl(string postId) {
            var media = await _instaApi.GetMediaByIdAsync(postId);
            if (!media.Succeeded || !media.Value.Images.Any()) {
                return null;
            }

            var images = media.Value.Images;
            if (images.Count > 1)
            {
                var maxWidth = images.Max(x => x.Width);
                images.RemoveAll(x => x.Width < maxWidth);
            }
            return images.First().URI;
        }
    }
}
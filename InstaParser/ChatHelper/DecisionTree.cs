﻿using System;
using System.Collections.Generic;
using System.Text;
using InstaParser.ChatHelper;

namespace InstaParser.ChatHelper
{
    public class Question
    {
        public string Text;
    }

    public static class DecisionTree
    {
        public static List<Question> Questions = new List<Question>()
        {
            new Question()
            {
                Text = "Добрый день!\n" +
                       "Мы обнаружили фотографию героя Вашей семьи на Вашей странице в Instagram.\n" +
                       "Чтобы увековечить память защитников нашего отечества, предлагаем помочь проекту \"Дорога памяти\", дополнив информацию о Герое.\n" +
                       "Согласны ли Вы на размещение фотографии вашего родственника и продолжение диалога с нами ?\n" +
                       "Подробнее о проекте на сайте pamyat-naroda.ru",
            },

            new Question()
            {
                Text = "Спасибо, что решили принять участие в важном для нашего народа деле.\n" +
                       "Фотография Героя будет размещена на сайте.\n" +
                       "Также просим уточнить следующую информацию:\n" +
                       "Откуда родом был Герой Вашей семьи?"
            },

            new Question()
            {
                Text = "Спасибо! Можете вкратце рассказать историю Вашего Героя?"
            },

            new Question()
            {
                Text = "Спасибо! Если история Вашего Героя упоминалась в других проекта, например, \"ОБД Мемориал\", \"ОБД Подвиг народа\", просьба прикрепить ссылку."
            },

            new Question()
            {
                Text = "Спасибо за принятое участие! Вечная память героям Великой Отечественной Войны!"
            },

            new Question()
            {
                Text = "Спасибо за уделенное внимание. Вы всегда можете вернуться и продолжить самостоятельно на сайте проекта."
            }
        };

        public static List<string> PositiveAnswers = new List<string>()
        {
            "Да",
            "Хорошо",
            "Давайте",
        };

        public static List<string> NegativeAnswers = new List<string>()
        {
            "Нет",
            "Не сейчас",
            "Позже",
            "Потом",
            "Не",
            "Не интересно"
        };
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InstaParser.Insta;
using InstaSharper.Classes.Models;

namespace InstaParser.ChatHelper
{
    public static class ChatHelper
    {
        //todo save user data
        public static string GetNextMessage(List<InstaDirectInboxItem> messages)
        {
            //находим последнее сообщение от бота
            var lastBotMsg = messages.FindLast(m => m.UserId == InstaConfig.BotId);

            //если еще ничего не отправляли
            if (lastBotMsg == null)
                return DecisionTree.Questions.First().Text;

            //находим последнее сообщение от польхователя
            var lastUserMsg = messages.FindLast(m => m.UserId != InstaConfig.BotId);

            //если послееднее сообщение было от пользователя, то должны ответить, иначе - ничего
            if (messages.IndexOf(lastUserMsg) > messages.IndexOf(lastBotMsg))
            {
                //проверяем, послал ли нас пользователь
                if (DecisionTree.NegativeAnswers.Select(a => a.ToLower()).Contains(lastUserMsg.Text.ToLower()))
                    return DecisionTree.Questions.Last().Text;

                var nextBotMsgInd = DecisionTree.Questions.IndexOf(DecisionTree.Questions.Find(q => q.Text == lastBotMsg.Text));
                nextBotMsgInd++;

                //если нечего больше отвечать, то не отвечаем
                if (nextBotMsgInd >= DecisionTree.Questions.Count)
                    return string.Empty;

                var nextBotMsg = DecisionTree.Questions.ElementAt(nextBotMsgInd);
                return nextBotMsg.Text;
            }

            return string.Empty;
        }
    }
}
